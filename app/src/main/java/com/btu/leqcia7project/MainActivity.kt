package com.btu.leqcia7project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        enterButton.setOnClickListener {
            if (emailAddressEditText.text.isEmpty() || editTextPassword.text.isEmpty()) {
                Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_LONG).show()
            }
            else if (emailAddressEditText.text.toString() == "aleksandre.kakhetelidze@btu.edu.ge" && editTextPassword.text.toString() == "paroli1") {
                val newIntent = Intent(this, ProfileActivity::class.java).apply {
                    putExtra("message", "gaumarjos")
                }
                startActivity(newIntent)
            } else {
                Toast.makeText(this, "Incorrect E-Mail or password", Toast.LENGTH_SHORT).show()
            }
        }
    }

}
